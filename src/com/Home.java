package com;

import java.util.ArrayList;
import java.util.List;

import com.Exception.InvalidSyntaxException;
import com.execute.Execution;

public class Home 
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args) 
	{
		/*Scanner sc=new Scanner(System.in);
		String query=sc.nextLine();
		sc.close();*/
		String query=args[0];
		query=query.toLowerCase();
		Home home=new Home();
		try
		{
			List<Object> temp=home.parseQuery(query);
			home.executeQuery((List<String>)temp.get(0), (List<String>)temp.get(1), (List<String>)temp.get(2), (String)temp.get(3));	
		}
		catch(Exception e)
		{
			System.out.println(e);
		}	
	}
	
	public List<Object> parseQuery(String query) throws Exception
	{
		List<String> selectList=new ArrayList<String>();
		List<String> fromList=new ArrayList<String>();
		List<String> whereList=new ArrayList<String>();
		String condition=new String();
		if(query.contains(" from "))
		{
			String arr[]=query.split(" from ");
			if(arr.length!=2)
				throw new InvalidSyntaxException("Invalid SQL syntax");
			if(arr[0].contains("select "))
			{
				String sl=arr[0].substring(7);
				sl=sl.trim();
				if(sl.startsWith("distinct "))
					selectList.add(sl);
				else if(sl.matches("distinct[(][A-Za-z0-9,]*[)]"))
					selectList.add(sl);
				else
				{
					String []tableList=sl.split(",");
					for(String s:tableList)
					{
						s=s.trim();
						if(s.isEmpty())
							throw new InvalidSyntaxException("Invalid SQL syntax");
						selectList.add(s.trim());
					}
				}	
			}
			else
				throw new InvalidSyntaxException("Invalid SQL syntax");
			if(arr[1].contains(" where "))
			{
				String srr[]=arr[1].split(" where ");
				if(srr.length!=2)
					throw new InvalidSyntaxException("Invalid SQL syntax");
				srr[0]=srr[0].trim();
				if(srr[0].isEmpty())
					throw new InvalidSyntaxException("Invalid SQL syntax");
				String []tableList=srr[0].split(",");
				for(String s:tableList)
				{
					s=s.trim();
					if(s.isEmpty())
						throw new InvalidSyntaxException("Invalid SQL syntax");
					fromList.add(s);
				}
				srr[1]=srr[1].trim();
				if(srr[1].isEmpty())
					throw new InvalidSyntaxException("Invalid SQL syntax");
				if(srr[1].contains(" and "))
				{
					condition="and";
					String []temp=srr[1].split(" and ");
					for(String s:temp)
					{
						s=s.trim();
						if(s.isEmpty())
							throw new InvalidSyntaxException("Invalid SQL syntax");
						whereList.add(s);
					}	
				}
				else if(srr[1].contains(" or "))
				{
					condition="or";
					String []temp=srr[1].split(" or ");
					for(String s:temp)
					{
						s=s.trim();
						if(s.isEmpty())
							throw new InvalidSyntaxException("Invalid SQL syntax");
						whereList.add(s);
					}	
				}	
				else
					whereList.add(srr[1]);
			}
			else
			{
				arr[1]=arr[1].trim();
				if(arr[1].isEmpty())
					throw new InvalidSyntaxException("Invalid SQL syntax");
				String []tableList=arr[1].split(",");
				for(String s:tableList)
				{
					s=s.trim();
					if(s.isEmpty())
						throw new InvalidSyntaxException("Invalid SQL syntax");
					fromList.add(s);
				}	
			}
		}
		else
			throw new InvalidSyntaxException("Invalid SQL syntax");
		List<Object> temp=new ArrayList<Object>();
		temp.add(selectList);
		temp.add(fromList);
		temp.add(whereList);
		temp.add(condition);
		return temp;
	}
	
	@SuppressWarnings("unchecked")
	public void executeQuery(List<String> selectList,List<String> fromList,List<String> whereList,String condition) throws Exception
	{
		if(selectList.isEmpty() || fromList.isEmpty())
			throw new InvalidSyntaxException("Invalid SQL syntax");
		Execution execution=new Execution();
		List<Object> list=execution.processFrom(fromList);
		List<List<String>> res=(List<List<String>>)list.get(0);
		List<String> cols=(List<String>)list.get(1);
		if(!whereList.isEmpty())
		{
			List<Integer> rowList=new ArrayList<Integer>();
			for(int i=0;i<res.size();i++)
				rowList.add(i);
			List<Object> resList=execution.processWhere(res, whereList, condition, rowList,cols,fromList);
			res=(List<List<String>>)resList.get(0);
			cols=(List<String>)resList.get(1);
		}
		res=execution.processSelect(res, selectList, cols,fromList);
		for(String s:res.get(0))
			System.out.print(s.toUpperCase()+"\t|\t");
		System.out.println();
		System.out.println("-----------------------------------------------------------------------------");
		for(int i=1;i<res.size();i++)
		{
			for(String s:res.get(i))
				System.out.print(s+"\t|\t");
			System.out.println();
		}	
	}
}
