package com.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.Exception.AmbiguousFieldException;
import com.Exception.InvalidFieldException;
import com.Exception.InvalidSyntaxException;
import com.Exception.InvalidTableException;

public class SQLUtility implements IConstant 
{
	public Map<String,List<String>> metaData;
	
	public List<List<String>> loadTable(String tableName) throws Exception
	{
		loadMetaData();
		List<List<String>> tableData=new ArrayList<List<String>>();
		if(metaData.containsKey(tableName))
		{
			try(BufferedReader br = new BufferedReader(new FileReader(tableName+".csv"))) 
			{
				String line;
				while ((line = br.readLine()) != null) 
					tableData.add(Arrays.asList(line.split(",")));
		    }
			catch (Exception e) 
			{
				throw e;
			}
		}
		else
			throw new InvalidTableException("No such table exist");
		return tableData;
	}
	
	public boolean isJoin(String c1,String c2)
	{
		String carr1[]=c1.split("\\.");
		String carr2[]=c2.split("\\.");
		if(!carr1[0].equalsIgnoreCase(carr2[0]))
			return true;
		return false;
	}
	
	public void loadMetaData() throws Exception
	{
		try(BufferedReader br = new BufferedReader(new FileReader("metadata.txt"))) 
		{
			metaData=new HashMap<String, List<String>>();
			String line;
			while ((line = br.readLine()) != null) 
			{
				if(line.equalsIgnoreCase("<begin_table>"))
				{
					line = br.readLine();
					String tableName=line;
					List<String> colList=new ArrayList<String>(); 
					while ((line = br.readLine())!=null && !line.equalsIgnoreCase("<end_table>"))
						colList.add(line);
					metaData.put(tableName, colList);
				}
			}
	    }
		catch (Exception e) 
		{
			throw e;
		}
	}
	
	public void isAmbigous(String condition,List<String> tableList) throws Exception
	{
		int index=-1;
		List<String> colList=new ArrayList<String>();
		SQLUtility sqlObj=new SQLUtility();
		sqlObj.loadMetaData();
		for(String tableName:tableList)
		{
			List<String> ct=sqlObj.metaData.get(tableName);
			for(String s:ct)
				colList.add(tableName+"."+s);
		}
		for(int i=0;i<colList.size();i++)
		{
			if(condition.equalsIgnoreCase(colList.get(i).split("\\.")[1]))
			{
				if(index!=-1)
					throw new AmbiguousFieldException("Ambiguous field exception "+condition);
				else
					index=i;
			}	
		}
		
	}
	
	public int getColIndex(List<String> colList,String condition,List<String> tableList) throws Exception
	{
		int index=-1;
		condition=condition.trim();
		if(condition.contains("."))
		{
			for(int i=0;i<colList.size();i++)
			{
				if(colList.get(i).equalsIgnoreCase(condition))
				{
					index=i;
					break;
				}	
			}	
		}
		else
		{
			isAmbigous(condition, tableList);
			for(int i=0;i<colList.size();i++)
			{
				if(condition.equalsIgnoreCase(colList.get(i).split("\\.")[1]))
				{
					if(index!=-1)
						throw new AmbiguousFieldException("Ambiguous field exception "+condition);
					else
						index=i;
				}	
			}	
		}
		if(index==-1)
			throw new InvalidFieldException("No such Field exist");
		return index;
	}
	
	public String getColName(String name)
	{
		String newName=new String();
		if(name.contains("."))
			newName=name.split("\\.")[1];
		else
			newName=name;
		return newName;
	}
	
	public List<Object> getComparison(String cond) throws Exception
	{
		List<Object> list=new ArrayList<Object>();
		String comp=new String();
		String cols[]=null;
		if(cond.contains("="))
		{
			comp="=";
			cols=cond.trim().split("=");
		}	
		else if(cond.contains("<"))
		{
			comp="<";
			cols=cond.trim().split("<");
		}
		else if(cond.contains(">"))
		{
			comp=">";
			cols=cond.trim().split(">");
		}
		else
			throw new InvalidSyntaxException("Invalid where condition");
		list.add(cols);
		list.add(comp);
		return list;
	}
	
	public boolean getValue(int x,int y,String comp)
	{
		if(comp.equalsIgnoreCase("="))
		{
			if(x==y)
				return true;
			else
				return false;
		}
		else if(comp.equalsIgnoreCase(">"))
		{
			if(x>y)
				return true;
			else
				return false;
		}
		else if(comp.equalsIgnoreCase("<"))
		{
			if(x<y)
				return true;
			else
				return false;
		}
		else
			return false;
	}
}
