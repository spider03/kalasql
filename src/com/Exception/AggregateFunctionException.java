package com.Exception;

public class AggregateFunctionException extends Exception
{
	private static final long serialVersionUID = 1L;

	public AggregateFunctionException(String message) 
	{
		super(message);
	}
}
