package com.Exception;

public class AmbiguousFieldException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public AmbiguousFieldException(String message) 
	{
		super(message);
	}
}