package com.Exception;

public class InvalidSelectException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InvalidSelectException(String message)
	{
		super(message);
	}
	
}
