package com.Exception;

public class InvalidTableException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	String message;
	
	public InvalidTableException(String message) 
	{
		super(message);
	}
}
