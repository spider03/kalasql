package com.execute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.Exception.AggregateFunctionException;
import com.Exception.InvalidSelectException;
import com.Exception.InvalidSyntaxException;
import com.utility.IConstant;
import com.utility.SQLUtility;

public class Execution 
{
	public List<Object> processFrom(List<String> tableList) throws Exception
	{
		List<Object> list=new ArrayList<Object>();
		List<String> cols=new ArrayList<String>();
		List<List<String>> resultTable=new ArrayList<List<String>>();
		SQLUtility sqlObj=new SQLUtility();
		for(String tableName:tableList)
		{
			List<List<String>> tempTable=sqlObj.loadTable(tableName);
			List<String> ct=sqlObj.metaData.get(tableName);
			for(String s:ct)
				cols.add(tableName+"."+s);
			if(resultTable.isEmpty())
				resultTable=tempTable;
			else
			{
				List<List<String>> rtTable=new ArrayList<List<String>>();
				for(List<String> row:resultTable)
				{
					for(List<String> trow:tempTable)
					{
						List<String> temp=new ArrayList<String>();
						for(String s:row)
							temp.add(s);
						for(String s:trow)
							temp.add(s);
						rtTable.add(temp);
					}	
				}
				resultTable=rtTable;
			}	
		}
		list.add(resultTable);
		list.add(cols);
		return list;
	}
	
	public List<Object> processWhere(List<List<String>> inputTable,List<String> conditions,String condition,List<Integer> rowList,List<String> columnsList,List<String> tableList) throws Exception
	{
		List<Integer> temp=rowList;
		SQLUtility sqlObj=new SQLUtility();
		Map<Integer,Integer> dropList=new HashMap<Integer,Integer>();
		if(condition.isEmpty())
		{
			for(String cond:conditions)
			{	
				if(cond.isEmpty())
					throw new InvalidSyntaxException("Invalid SQL syntax");
				List<Object> compD=sqlObj.getComparison(cond);
				String cols[]=(String [])compD.get(0);
				String comp=(String)compD.get(1);
				if(cols.length!=2)
					throw new InvalidSyntaxException("Invalid where condition");
				cols[0]=cols[0].trim();
				cols[1]=cols[1].trim();
				int c1=sqlObj.getColIndex(columnsList, cols[0],tableList),c2;
				boolean flag=false;
				if(cols[1].matches("[a-z][a-zA-Z.0-9]*"))
					c2=sqlObj.getColIndex(columnsList, cols[1],tableList);
				else
				{	
					flag=true;
					if(!cols[1].matches("[0-9]*"))
						throw new InvalidSyntaxException("Invalid where condition");
					if(cols[1].charAt(0)=='-')
					{
						cols[1]=cols[1].substring(1);
						c2=Integer.parseInt(cols[1]);
						c2=(-1)*c2;
					}
					else
						c2=Integer.parseInt(cols[1]);
				}	
				List<Integer> conditionNum=new ArrayList<Integer>();
				for(int rowNum:temp)
				{
					List<String> row=inputTable.get(rowNum);
					if(flag)
					{
						if(sqlObj.getValue(Integer.parseInt(row.get(c1)),c2, comp))
							conditionNum.add(rowNum);
					}	
					else if(sqlObj.getValue(Integer.parseInt(row.get(c1)),Integer.parseInt(row.get(c2)), comp))
							conditionNum.add(rowNum);	
				}
				if(!flag && sqlObj.isJoin(columnsList.get(c1), columnsList.get(c2)))
					dropList.put(c2,1);
				temp=conditionNum;
			}
		}	
		else if(condition.equalsIgnoreCase(IConstant.conEnum.AND.toString()))
		{
			for(String cond:conditions)
			{	
				if(cond.isEmpty())
					throw new InvalidSyntaxException("Invalid SQL syntax");
				List<Object> compD=sqlObj.getComparison(cond);
				String cols[]=(String [])compD.get(0);
				String comp=(String)compD.get(1);
				if(cols.length!=2)
					throw new InvalidSyntaxException("Invalid where condition");
				cols[0]=cols[0].trim();
				cols[1]=cols[1].trim();
				int c1=sqlObj.getColIndex(columnsList, cols[0],tableList),c2;
				boolean flag=false;
				if(cols[1].matches("[a-z][a-zA-Z.0-9]*"))
					c2=sqlObj.getColIndex(columnsList, cols[1],tableList);
				else
				{	
					flag=true;
					if(!cols[1].matches("[0-9-]*"))
						throw new InvalidSyntaxException("Invalid where condition");
					if(cols[1].charAt(0)=='-')
					{
						cols[1]=cols[1].substring(1);
						c2=Integer.parseInt(cols[1]);
						c2=(-1)*c2;
					}
					else
						c2=Integer.parseInt(cols[1]);
				}
				List<Integer> conditionNum=new ArrayList<Integer>();
				for(int rowNum:temp)
				{
					List<String> row=inputTable.get(rowNum);
					if(flag)
					{
						if(sqlObj.getValue(Integer.parseInt(row.get(c1)),c2, comp))
							conditionNum.add(rowNum);
					}	
					else if(sqlObj.getValue(Integer.parseInt(row.get(c1)),Integer.parseInt(row.get(c2)), comp))
						conditionNum.add(rowNum);
				}
				if(!flag && sqlObj.isJoin(columnsList.get(c1), columnsList.get(c2)))
					dropList.put(c2,1);
				temp=conditionNum;
			}
		}
		else
		{
			Set<Integer> colList=new HashSet<Integer>();
			for(String cond:conditions)
			{
				if(cond.isEmpty())
					throw new InvalidSyntaxException("Invalid SQL syntax");
				List<Object> compD=sqlObj.getComparison(cond);
				String cols[]=(String [])compD.get(0);
				String comp=(String)compD.get(1);
				if(cols.length!=2)
					throw new InvalidSyntaxException("Invalid where condition");
				cols[0]=cols[0].trim();
				cols[1]=cols[1].trim();
				int c1=sqlObj.getColIndex(columnsList, cols[0],tableList),c2;
				boolean flag=false;
				if(cols[1].matches("[a-z][a-zA-Z.0-9]*"))
					c2=sqlObj.getColIndex(columnsList, cols[1],tableList);
				else
				{	
					flag=true;
					if(!cols[1].matches("[0-9]*"))
						throw new InvalidSyntaxException("Invalid where condition");
					if(cols[1].charAt(0)=='-')
					{
						cols[1]=cols[1].substring(1);
						c2=Integer.parseInt(cols[1]);
						c2=(-1)*c2;
					}
					else
						c2=Integer.parseInt(cols[1]);
				}
				for(int rowNum:temp)
				{
					List<String> row=inputTable.get(rowNum);
					if(flag)
					{
						if(sqlObj.getValue(Integer.parseInt(row.get(c1)),c2, comp))
							colList.add(rowNum);
					}	
					else if(sqlObj.getValue(Integer.parseInt(row.get(c1)),Integer.parseInt(row.get(c2)), comp))
						colList.add(rowNum);
				}
				if(!flag && sqlObj.isJoin(columnsList.get(c1), columnsList.get(c2)))
					dropList.put(c2,1);
			}
			temp=new ArrayList<Integer>();
			for(int x:colList)
				temp.add(x);
		}
		List<List<String>> res=new ArrayList<List<String>>();
		for(int t:temp)
		{
			List<String> row=inputTable.get(t);
			List<String> nrow=new ArrayList<String>();
			for(int i=0;i<row.size();i++)
			{
				if(!dropList.containsKey(i))
					nrow.add(row.get(i));	
			}
			res.add(nrow);
		}
		List<Object> cols=new ArrayList<Object>();
		for(int i=0;i<columnsList.size();i++)
		{
			if(!dropList.containsKey(i))
				cols.add(columnsList.get(i));
		}	
		List<Object> list=new ArrayList<Object>();
		list.add(res);
		list.add(cols);
		return list;
	}
	
	public List<List<String>> processSelect(List<List<String>> inputTable,List<String> selectConditions,List<String> columnsList,List<String> tableList) throws Exception
	{
		List<List<String>> colTable=new ArrayList<List<String>>();
		SelectOperation sobj=new SelectOperation();
		int min=inputTable.size();
		boolean nFlag=false;
		boolean aFlag=false;
		for(String cond:selectConditions)
		{
			if(cond.startsWith(IConstant.keywordEnum.distinct.toString()+" "))
			{
				if(selectConditions.size()>1)
					throw new InvalidSelectException("Distinct cannot be grouped with other fields");
				else
				{
					SelectOperation stobj=new SelectOperation();
					String x[]=cond.split(" ");
					if(x.length!=2)
						throw new InvalidSyntaxException("Syntax error near distinct");
					List<List<String>> list=stobj.processDistinct(x[1], columnsList, inputTable,tableList);
					for(List<String> temp:list)
						colTable.add(temp);
					min=colTable.get(colTable.size()-1).size()-1;
				}	
			}
			else if(cond.matches("distinct[(][A-Za-z0-9,]*[)]"))
			{
				if(selectConditions.size()>1)
					throw new InvalidSelectException("Distinct cannot be grouped with other fields");
				else
				{
					SelectOperation stobj=new SelectOperation();
					cond=cond.substring(cond.indexOf("(")+1, cond.indexOf(")"));
					List<List<String>> list=stobj.processDistinct(cond, columnsList, inputTable,tableList);
					for(List<String> temp:list)
						colTable.add(temp);
					min=colTable.get(colTable.size()-1).size()-1;
				}
			}	
			else if(cond.matches("sum[(][A-Za-z0-9]*[)]"))
			{
				if(nFlag)
					throw new AggregateFunctionException("Aggregate function cannot be grouped with other fields");
				min=1;
				SelectOperation stobj=new SelectOperation();
				colTable.add(stobj.processSum(cond, columnsList, inputTable,tableList));
				aFlag=true;
			}
			else if(cond.matches("avg[(][A-Za-z0-9]*[)]"))
			{
				if(nFlag)
					throw new AggregateFunctionException("Aggregate function cannot be grouped with other fields");
				min=1;
				SelectOperation stobj=new SelectOperation();
				colTable.add(stobj.processAvg(cond, columnsList, inputTable,tableList));
				aFlag=true;
			}
			else if(cond.matches("max[(][A-Za-z0-9]*[)]"))
			{
				if(nFlag)
					throw new AggregateFunctionException("Aggregate function cannot be grouped with other fields");
				min=1;
				SelectOperation stobj=new SelectOperation();
				colTable.add(stobj.processMax(cond, columnsList, inputTable,tableList));
				aFlag=true;
			}
			else if(cond.matches("min[(][A-Za-z0-9]*[)]"))
			{
				if(nFlag)
					throw new AggregateFunctionException("Aggregate function cannot be grouped with other fields");
				min=1;
				SelectOperation stobj=new SelectOperation();
				colTable.add(stobj.processMin(cond, columnsList, inputTable,tableList));
				aFlag=true;
			}
			else if(cond.equalsIgnoreCase("*"))
			{
				if(aFlag)
					throw new AggregateFunctionException("Aggregate function cannot be grouped with other fields");
				SelectOperation stobj=new SelectOperation();
				for(String col:columnsList)
					colTable.add(stobj.getColList(col, columnsList, inputTable,tableList));
				nFlag=true;
			}
			else
			{
				if(aFlag)
					throw new AggregateFunctionException("Aggregate function cannot be grouped with other fields");
				SelectOperation stobj=new SelectOperation();
				colTable.add(stobj.getColList(cond, columnsList, inputTable,tableList));
				nFlag=true;
			}	
		}
		List<List<String>> resultTable=sobj.arrangeRowWise(min, colTable);
		return resultTable;
	}
}
