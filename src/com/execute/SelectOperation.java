package com.execute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.Exception.InvalidSelectException;
import com.utility.SQLUtility;

public class SelectOperation 
{
	public List<List<String>> processDistinct(String cond, List<String> columnsList,List<List<String>> inputTable,List<String> tableList) throws Exception
	{
		List<List<String>> list=new ArrayList<List<String>>();
		SQLUtility sqlObj=new SQLUtility();
		if(cond.contains(","))
		{
			String cols[]=cond.split(",");
			int arr[]=new int[cols.length];
			for(int i=0;i<cols.length;i++)
				arr[i]=sqlObj.getColIndex(columnsList, cols[i],tableList);
			Set<List<String>> valueSet=new HashSet<List<String>>();
			for(List<String> row:inputTable)
			{
				List<String> vList=new ArrayList<String>();
				for(int x:arr)
					vList.add(row.get(x));
				valueSet.add(vList);
			}
			for(int i=0;i<cols.length;i++)
			{
				List<String> l=new ArrayList<String>();
				l.add(sqlObj.getColName(cols[i].trim()));
				for(List<String> m:valueSet)
					l.add(m.get(i));
				list.add(l);	
			}	
		}	
		else
		{
			Set<String> columnValues=new HashSet<String>();
			List<String> columnValuesList=new ArrayList<String>();
			int index=-1;
			index=sqlObj.getColIndex(columnsList, cond,tableList);
			if(index==-1)
				throw new InvalidSelectException("No such column exist");
			else
			{
				for(List<String> row:inputTable)
					columnValues.add(row.get(index));
				columnValuesList.add(sqlObj.getColName(cond));
				for(String x:columnValues)
					columnValuesList.add(x);
			}
			list.add(columnValuesList);
		}	
		return list;
	}
	
	
	
	public List<String> processSum(String cond, List<String> columnsList,List<List<String>> inputTable,List<String> tableList) throws Exception
	{
		List<String> columnValuesList=new ArrayList<String>();
		int index=-1;
		String colName=cond.substring(cond.indexOf("(")+1, cond.indexOf(")"));
		SQLUtility sqlObj=new SQLUtility();
		index=sqlObj.getColIndex(columnsList, colName,tableList);
		long sum=0;
		for(List<String> row:inputTable)
			sum+=Integer.parseInt(row.get(index));
		columnValuesList.add("sum("+sqlObj.getColName(colName)+")");
		columnValuesList.add(Long.toString(sum));
		return columnValuesList;
	}
	
	public List<String> processAvg(String cond, List<String> columnsList,List<List<String>> inputTable,List<String> tableList) throws Exception
	{
		List<String> columnValuesList=new ArrayList<String>();
		int index=-1;
		String colName=cond.substring(cond.indexOf("(")+1, cond.indexOf(")"));
		SQLUtility sqlObj=new SQLUtility();
		index=sqlObj.getColIndex(columnsList, colName,tableList);
		long sum=0,i=0;
		for(List<String> row:inputTable)
		{
			sum+=Integer.parseInt(row.get(index));
			i++;
		}	
		columnValuesList.add("avg("+sqlObj.getColName(colName)+")");
		double avg=(double)sum/(double)i;
		columnValuesList.add(Double.toString(avg));
		return columnValuesList;
	}
	
	public List<String> processMin(String cond, List<String> columnsList,List<List<String>> inputTable,List<String> tableList) throws Exception
	{
		List<String> columnValuesList=new ArrayList<String>();
		int index=-1;
		String colName=cond.substring(cond.indexOf("(")+1, cond.indexOf(")"));
		SQLUtility sqlObj=new SQLUtility();
		index=sqlObj.getColIndex(columnsList, colName,tableList);
		int min=Integer.MAX_VALUE;
		for(List<String> row:inputTable)
		{
			int x=Integer.parseInt(row.get(index));
			if(min>x)
				min=x;
		}
		columnValuesList.add("min("+sqlObj.getColName(colName)+")");
		columnValuesList.add(Integer.toString(min));
		return columnValuesList;
	}
	
	public List<String> processMax(String cond, List<String> columnsList,List<List<String>> inputTable,List<String> tableList) throws Exception
	{
		List<String> columnValuesList=new ArrayList<String>();
		int index=-1;
		String colName=cond.substring(cond.indexOf("(")+1, cond.indexOf(")"));
		SQLUtility sqlObj=new SQLUtility();
		index=sqlObj.getColIndex(columnsList, colName,tableList);
		int max=Integer.MIN_VALUE;
		for(List<String> row:inputTable)
		{
			int x=Integer.parseInt(row.get(index));
			if(max<x)
				max=x;
		}
		columnValuesList.add("max("+sqlObj.getColName(colName)+")");
		columnValuesList.add(Integer.toString(max));
		return columnValuesList;
	}
	
	public List<String> getColList(String cond, List<String> columnsList,List<List<String>> inputTable,List<String> tableList) throws Exception
	{
		List<String> columnValuesList=new ArrayList<String>();
		int index=-1;
		String colName=cond;
		SQLUtility sqlObj=new SQLUtility();
		index=sqlObj.getColIndex(columnsList, colName,tableList);
		columnValuesList.add(sqlObj.getColName(colName));
		for(List<String> row:inputTable)
			columnValuesList.add(row.get(index));	
		return columnValuesList;
	}
		
	public List<List<String>> arrangeRowWise(int min,List<List<String>> column)
	{
		List<List<String>> resultTable=new ArrayList<List<String>>();
		for(int i=0;i<=min;i++)
		{
			List<String> row=new ArrayList<String>();
			for(List<String> cl:column)
				row.add(cl.get(i));
			resultTable.add(row);
		}	
		return resultTable;
	}
}
